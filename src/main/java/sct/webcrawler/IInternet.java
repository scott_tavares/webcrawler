package sct.webcrawler;

import java.io.IOException;
import org.json.JSONObject;

/**
 * Rudimentary Web Crawler
 * Functional Spec. https://s3.amazonaws.com/ge.inet1.foo.bar.com/web-crawler-project.pdf

 Used to simulate an IInternet
 *
 * @author stavares <https://bitbucket.org/scott_tavares/>
 */
public interface IInternet {

//  Map<String, JSONObject> getPages() throws IOException;

  JSONObject request(String URL) throws IOException;

  public String getRootUrl();

}
