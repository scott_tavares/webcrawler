package sct.webcrawler;

import sct.webcrawler.spider.Spider;
import sct.webcrawler.spider.Web;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Rudimentary Web Crawler Functional Spec.
 * https://s3.amazonaws.com/ge.inet1.foo.bar.com/web-crawler-project.pdf
 *
 * @author stavares <https://bitbucket.org/scott_tavares/>
 */
public class WebCrawler {

  private static final Logger LOG = Logger.getLogger(WebCrawler.class.getName());

  private static final String sURL1 = "https://s3.amazonaws.com/ge.inet1.foo.bar.com/internet1.json";
  private static final String sURL2 = "https://s3.amazonaws.com/ge.inet1.foo.bar.com/internet2.json";

  /**
   * @param args the command line arguments
   * @throws java.io.IOException
   */
  public static void main(String[] args) throws IOException {
    ////////////////////////////////////////////////////////////////////////////
    // IInternet 1
    Spider spider1 = new Spider(new Web(Internet.getInstance(sURL1)),
        "http://foo.bar.com/p1");
    new Thread(spider1).start();

    ////////////////////////////////////////////////////////////////////////////
    // IInternet 2
    Spider spider2 = new Spider(new Web(Internet.getInstance(sURL2)),
        "http://foo.bar.com/p1");
    new Thread(spider2).start();

  }
}
