package sct.webcrawler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.jcip.annotations.Immutable;
import net.jcip.annotations.ThreadSafe;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;

/**
 * Rudimentary Web Crawler
 * Functional Spec. https://s3.amazonaws.com/ge.inet1.foo.bar.com/web-crawler-project.pdf
 *
 * @author stavares <https://bitbucket.org/scott_tavares/>
 */
@ThreadSafe
@Immutable
public class Internet implements IInternet {

  private final String rootUrl;
  private final Map<String, JSONObject> webPages;

  private Internet(String rootUrl) throws IOException {
    this.rootUrl = rootUrl;
    this.webPages = new HashMap<>();
    String json = Jsoup.connect(this.rootUrl).ignoreContentType(true).execute().body();
    JSONObject obj = new JSONObject(json);
    JSONArray pageObjects = obj.getJSONArray("pages");

    for (int i = 0; i < pageObjects.length(); i++) {
      webPages.put(pageObjects.getJSONObject(i).getString("address"),
          pageObjects.getJSONObject(i));
    }
  }

  public static synchronized IInternet getInstance(String sURL) throws IOException {
    return new Internet(sURL);
  }

  @Override
  public JSONObject request(String URL) throws IOException {
    if(this.webPages.containsKey(URL))
      return this.webPages.get(URL);
    else
      throw new IOException("404 Error: " + URL + " not found!");
  }

  /**
   * @return the rootUrl
   */
  @Override
  public String getRootUrl() {
    return new String(rootUrl);
  }
}
