package sct.webcrawler.spider;

import sct.webcrawler.IInternet;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 * This class represents a thread safe class for all shared data in a
 * multi-threaded environment.
 *
 * @author stavares
 */
public class Web {

  private static final Logger LOG = Logger.getLogger(Web.class.getName());

  // Fields
  private final IInternet internet;

  private final int BOUND = 100; // put this into a yaml property file

  // A list is being used bcause it holds orger (add to end) and
  // is allows dublicate values. This will simulte a breth first
  // search (BFS).
  private final BlockingDeque<String> pagesToVisit = new LinkedBlockingDeque<>(BOUND);

//  // A set is being used for pages that have been visited
//  // because a set will not hold dublicate values
  private final Set<String> pagesVisited = ConcurrentHashMap.newKeySet();

  public Web(IInternet internet) {
    this.internet = internet;
  }

  public void putPageToVisit(String pageToVisit) {
    try {
      pagesToVisit.putLast(pageToVisit);
    } catch (InterruptedException ex) {
      Logger.getLogger(Web.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public String takePageToVisit() throws InterruptedException {
    try {
      return pagesToVisit.takeFirst();
    } catch (InterruptedException ex) {
      Logger.getLogger(Web.class.getName()).log(Level.SEVERE, null, ex);
      throw ex;
    }
    //return null;
  }

  public boolean hasPagesToVisit() {
    return !pagesToVisit.isEmpty();
  }

  public JSONObject request(String URL) throws IOException {
    return this.internet.request(URL);
  }

  public String getRootUrl() {
    return this.internet.getRootUrl();
  }

  boolean pagesVisitedContains(String currentUrl) {
    return this.pagesVisited.contains(currentUrl);
  }

  void pagesVisitedAdd(String currentUrl) {
    this.pagesVisited.add(currentUrl);
  }

  void pagesVisitedForEach() {
    this.pagesVisited.forEach((v) -> {
      System.out.println("Page: " + v);
    });
  }
}
