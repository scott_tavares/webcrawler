package sct.webcrawler.spider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import net.jcip.annotations.Immutable;
import net.jcip.annotations.ThreadSafe;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Rudimentary Web Crawler Functional Spec.
 * https://s3.amazonaws.com/ge.inet1.foo.bar.com/web-crawler-project.pdf
 *
 * This class retrieves a page from an IInternet then it parses it for 'links'
 * and stores them.
 *
 * @author stavares <https://bitbucket.org/scott_tavares/>
 */
@ThreadSafe
@Immutable
class Crawler {

  private final List<String> links = new ArrayList<>();

  /**
   *
   * @param url The page to retrieve.
   * @param web the IInternet to request the page from.
   * @return true if the page is found, false if the page is not found.
   * @throws java.io.IOException
   */
  List<String> crawl(String url, Web web) throws IOException {

    // MUST clear the previous page's links!!!!
    this.links.clear();
    JSONObject page = web.request(url); // Throws IOException if page not found.

    // Add Page Links
    JSONArray pageLinks = page.getJSONArray("links");
    for (int i = 0; i < pageLinks.length(); i++) {
      this.links.add(pageLinks.getString(i));
    }
    
    return Collections.unmodifiableList(this.links.stream()
        .collect(Collectors.toList()));
  }
}
