package sct.webcrawler.spider;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.jcip.annotations.ThreadSafe;

/**
 * Rudimentary Web Crawler Functional Spec.
 * https://s3.amazonaws.com/ge.inet1.foo.bar.com/web-crawler-project.pdf
 *
 * This class is used to 'spider' or traverse the 'pages' of an IInternet. It
 * uses the Crawler.java class to 'crawl' each individual 'page' for 'links' by
 * first requesting the page from an IInternet class then parsing it for other
 * links to spider.
 *
 * @author stavares <https://bitbucket.org/scott_tavares/>
 */
@ThreadSafe
public class Spider implements Runnable {

  private static final Logger LOG = Logger.getLogger(Spider.class.getName());

  // Fields
  private final Web web;

  // List of duplicate pages
  // TODO: Move to the Web class
  private final Set<String> pagesSkiped = new HashSet<>();

  // Pages not found in the internet
  // TODO: Move to the Web class
  private final Set<String> pageErrors = new HashSet<>();

  private final String rootUrl;

  public Spider(Web web, String rootUrl) {
    this.web = web;
    this.rootUrl = rootUrl;
  }

  /**
   * +++ This code is an DFS Algorithm. +++
   *
   * @param url
   * @param internet
   */
  private void spider(String url) throws InterruptedException {
    String currentUrl;
    List<String> listOfLinks;

    do {

      if (web.hasPagesToVisit()) {
        currentUrl = this.nextUrl();
      } else {
        currentUrl = url; // Initial URL
      }

      try {
        // Only crawl and add links if the source page has NOT been visited yet.
        // TODO: Move this into the Crawler class
        if (!this.web.pagesVisitedContains(currentUrl)) {
          listOfLinks = new Crawler().crawl(currentUrl, this.web);
          listOfLinks.forEach(v -> {
            web.putPageToVisit(v);
          });
        }

        this.web.pagesVisitedAdd(currentUrl);
      } catch (IOException e) {
        pageErrors.add(currentUrl);
      }

    } while (web.hasPagesToVisit());
    report();
  }

  /**
   * This method will find the next URL to Crawl each time it called and add it
   * to the list of visited pages if it has NOT already been visited. It also
   * keeps track of every 'skiped' pages that have already been visited.
   *
   * @return the next URL to Crawler as a string
   */
  private String nextUrl() throws InterruptedException {

    String nextUrl;
    nextUrl = web.takePageToVisit();

    while (web.pagesVisitedContains(nextUrl)) {
      pagesSkiped.add(nextUrl); // 6.e.ii

      if (web.hasPagesToVisit()) {
        nextUrl = web.takePageToVisit();
      } else {
        // NO MORE PAGES TO VISIT
        break;
      }
    }
    return nextUrl;
  }

  @Override
  public void run() {
    try {
      this.spider(this.rootUrl);
    } catch (InterruptedException ex) {
      Logger.getLogger(Spider.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public synchronized void report() {
    System.out.println("\n\nINTERNET: " + web.getRootUrl());
    System.out.println("");
    System.out.println("======================================");
    System.out.println("PAGES VISITED");
    this.web.pagesVisitedForEach();
    System.out.println("======================================");
    System.out.println("PAGES Skiped");
    this.pagesSkiped.forEach((v) -> {
      System.out.println("Page: " + v);
    });
    System.out.println("======================================");
    System.out.println("PAGES ERRORS");
    this.pageErrors.forEach((v) -> {
      System.out.println("Error: " + v);
    });
  }

}
